"""REST client handling, including MparticleStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional

from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BearerTokenAuthenticator
from time import time


class MparticleStream(RESTStream):
    """Mparticle stream class."""
    
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["api_url"]

    records_jsonpath = "$.data[*]"  
    access_token = None
    expires_in = None

    def get_token(self):
        now = round(time())
        if (not self.access_token or (not self.expires_in) or ((self.expires_in - now) < 60)):
            s = requests.Session()
            payload = {
                "client_id": self.config.get("client_id"),
                "client_secret": self.config.get("client_secret"),
                "audience": "https://api.mparticle.com",
                "grant_type": "client_credentials"
            }
            login = s.post("https://sso.auth.mparticle.com/oauth/token", json=payload).json()
            self.access_token = login["access_token"]
            self.expires_in = login["expires_in"] + now
        return self.access_token

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(
            self,
            token=self.get_token()
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        next_page_token = None
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params['accountId'] = self.config.get('account_id')
        return params

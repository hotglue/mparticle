"""Mparticle tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_mparticle.streams import (
    MparticleStream,
    AccountsStream,
    AppsStream,
    AudiencesStream,
    DatapointsStream,
)

STREAM_TYPES = [
    AccountsStream,
    AppsStream,
    AudiencesStream,
    DatapointsStream,
]


class TapMparticle(Tap):
    """Mparticle tap class."""
    name = "tap-mparticle"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),     
        th.Property(
            "account_id",
            th.NumberType,
            required=True,
        ),     
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.mparticle.com/v1",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == '__main__':
    TapMparticle.cli()

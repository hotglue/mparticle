"""Stream type classes for tap-mparticle."""

from pathlib import Path
from typing import Optional

from singer_sdk import typing as th 

from tap_mparticle.client import MparticleStream


class AccountsStream(MparticleStream):
    """Define Accounts stream."""
    name = "accounts"
    path = "/accounts"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("last_modified_on", th.DateTimeType),
        th.Property("created_on", th.DateTimeType),
        th.Property("data_type", th.StringType),
        
    ).to_dict()

class AppsStream(MparticleStream):
    """Define apps stream."""
    name = "apps"
    path = "/apps"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("last_modified_on", th.DateTimeType),
        th.Property("created_on", th.DateTimeType),
        th.Property("data_type", th.StringType),
        th.Property("platforms", th.CustomType({"type": ["array", "string"]})),
        
    ).to_dict()
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "app_id": record["id"],
        }

class AudiencesStream(MparticleStream):
    """Define audiences stream."""
    name = "audiences"
    path = "/audiences"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("external_name", th.StringType),
        th.Property("size", th.NumberType),
        th.Property("created_by", th.StringType),
        th.Property("last_modified_by", th.StringType),
        th.Property("last_modified_on", th.DateTimeType),
        th.Property("is_calculated", th.BooleanType),
        th.Property("added_last_24_hours", th.NumberType),
        th.Property("dropped_last_24_hours", th.NumberType),
        th.Property("status", th.StringType),
        th.Property("connected_outputs", th.CustomType({"type": ["array", "string"]})),
        th.Property("workspaces", th.CustomType({"type": ["array", "string"]})),
        th.Property("last_modified_on", th.DateTimeType),
        th.Property("created_on", th.DateTimeType),
        th.Property("data_type", th.StringType),
              
    ).to_dict()

class DatapointsStream(MparticleStream):
    """Define audiences stream."""
    name = "data_points"
    path = "/apps/{app_id}/datapoints"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = AppsStream
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("attribute_name", th.StringType),
        th.Property("type", th.StringType),
        th.Property("event_type", th.StringType),
        th.Property("data_type", th.StringType),
        th.Property("created_on", th.DateTimeType),
              
    ).to_dict()
